@extends('panel.layout')

@section('contenido')
    <div class="row">
        <!-- ============================================================== -->
        <!-- basic table -->
        <!-- ============================================================== -->
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="float-left">Tareas</h5> 
                    <a href="{{ route('tareas.create') }}" class="btn btn-success btn-xs float-right"><i class="fa fa-plus-circle"></i> Crear</a>
                </div>
                <div class="card-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Titulo</th>
                                <th scope="col">Descripcion</th>
                                <th scope="col">Prioridad</th>
                                <th scope="col">Fase</th>
                                <th scope="col">Tipo</th>
                                <th scope="col">Fecha Inicial</th>
                                <th scope="col">Fecha Final</th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($tareas as $tarea)
                                <tr>
                                    <th scope="row">{{ $tarea->id }} </th>
                                    <td>{{ $tarea->titulo }}</td>
                                    <td>
                                        <form action="">
                                            <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" readonly>{{ $tarea->descripcion }}</textarea>
                                        </form>
                                    </td>
                                    <td>{{ $tarea->prioridad }}</td>
                                    <td>{{ $tarea->fase }}</td>
                                    <td>{{ $tarea->tipo }}</td>
                                    <td> {{ date('d-m-Y', strtotime($tarea->fecha_inicio)) }} </td>
                                    <td> {{ date('d-m-Y', strtotime($tarea->fecha_final)) }} </td>
                                    <td>
                                        <a href="{{ route('tareas.edit', ['id' => $tarea->id]) }}" class="btn btn-primary btn-xs"># Editar</a>
                                        <form class="d-inline" action="{{ route('tareas.destroy', ['id' => $tarea->id]) }}" method="POST">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}
                                            <button type="submit" class="btn btn-secondary btn-xs">Eliminar</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end basic table -->
        <!-- ============================================================== -->
    </div>
@endsection