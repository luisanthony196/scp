@extends('panel.layout')

@section('contenido')
    <div class="row">
        <!-- ============================================================== -->
        <!-- basic form -->
        <!-- ============================================================== -->
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="card">
                <h5 class="card-header">Editar Tarea</h5>
                <div class="card-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <h6>Por favor corrige los errores debajo:</h6>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form method='POST' action="{{ url('tarea/'.$tarea->id) }}" id="basicform" data-parsley-validate="">
                        {{ method_field('PUT') }}
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="inputTitulo">Titulo</label>
                            <input value="{{ old('titulo', $tarea->titulo) }}" id="inputTitulo" type="text" name="titulo" data-parsley-trigger="change" placeholder="Titulo" autocomplete="off" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="inputDescripcion">Descripción</label>
                            <textarea class="form-control" name="descripcion" id="inputDescripcion" rows="3">{{ old('descripcion', $tarea->descripcion) }}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="inputPrioridad">Prioridad</label>
                            <select class="form-control" id="inputPrioridad" name="prioridad">
                                @switch($tarea->prioridad)
                                    @case('Bajo')
                                        <option selected="selected">Bajo</option>
                                        <option>Medio</option>
                                        <option>Alto</option>
                                        @break

                                    @case('Medio')
                                        <option>Bajo</option>
                                        <option selected="selected">Medio</option>
                                        <option>Alto</option>
                                        @break

                                    @default
                                        <option>Bajo</option>
                                        <option>Medio</option>
                                        <option selected="selected">Alto</option>
                                @endswitch
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="inputFase">Fase</label>
                            <input value="{{ old('fase', $tarea->fase) }}" id="inputFase" type="text" name="fase" data-parsley-trigger="change" placeholder="Fase" autocomplete="off" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="inputTipo">Tipo</label>
                            <select class="form-control" id="inputTipo" name="tipo">
                                @switch($tarea->tipo)
                                    @case('Independiente')
                                        <option selected="selected">Independiente</option>
                                        <option>Secuencial</option>
                                        @break

                                    @default
                                        <option>Independiente</option>
                                        <option selected="selected">Secuencial</option>
                                @endswitch
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="inputFechaInicio">Fecha de Inicio</label>
                            <div class="input-group date" id="fechaInicio" data-target-input="nearest">
                                <input value="{{ old('fecha_inicio', $tarea->fecha_inicio) }}" id="inputFechaInicio" type="text" name="fecha_inicio"  class="form-control datetimepicker-input" data-target="#fechaInicio">
                                <div class="input-group-append" data-target="#fechaInicio" data-toggle="datetimepicker">
                                    <div class="input-group-text"><i class="far fa-calendar-alt"></i></div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputFechaFinal">Fecha de Final</label>
                            <div class="input-group date" id="fechaFinal" data-target-input="nearest">
                                <input value="{{ old('fecha_final', $tarea->fecha_final) }}" id="inputFechaFinal" type="text" name="fecha_final"  class="form-control datetimepicker-input" data-target="#fechaFinal">
                                <div class="input-group-append" data-target="#fechaFinal" data-toggle="datetimepicker">
                                    <div class="input-group-text"><i class="far fa-calendar-alt"></i></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 pl-0">
                                <p class="text-right">
                                    <button type="submit" class="btn btn-space btn-primary">Submit</button>
                                    <button class="btn btn-space btn-secondary"><a href="{{ route('usuarios.index') }}">Cancel</a></button>
                                </p>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end basic form -->
        <!-- ============================================================== -->
    </div>
@endsection