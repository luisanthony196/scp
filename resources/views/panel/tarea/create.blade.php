@extends('panel.layout')

@section('contenido')
    <div class="row">
        <!-- ============================================================== -->
        <!-- basic form -->
        <!-- ============================================================== -->
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="card">
                <h5 class="card-header">Tarea</h5>
                <div class="card-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <h6>Por favor corrige los errores debajo:</h6>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form method='POST' action="{{ url('tareas') }}" id="basicform" data-parsley-validate="">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="inputTitulo">Titulo</label>
                            <input id="inputTitulo" type="text" name="titulo" value="{{ old('titulo') }}" data-parsley-trigger="change" placeholder="Titulo" autocomplete="off" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="inputDescripcion">Descripción</label>
                            <textarea class="form-control" name="descripcion" id="inputDescripcion" rows="3">
                                {{ old('descripcion') }}
                            </textarea>
                        </div>
                        <div class="form-group">
                            <label for="inputPrioridad">Prioridad</label>
                            <select class="form-control" id="inputPrioridad" name="prioridad">
                                @switch(old('prioridad'))
                                    @case('Medio')
                                        <option>Bajo</option>
                                        <option selected>Medio</option>
                                        <option>Alto</option>
                                        @break

                                    @case('Alto')
                                        <option>Bajo</option>
                                        <option>Medio</option>
                                        <option selected>Alto</option>
                                        @break

                                    @default
                                        <option selected>Bajo</option>
                                        <option>Medio</option>
                                        <option>Alto</option>
                                @endswitch
                                <option>Bajo</option>
                                <option>Medio</option>
                                <option>Alto</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="inputFase">Fase</label>
                            <input id="inputFase" type="text" name="fase" value="{{ old('fase') }}" data-parsley-trigger="change" placeholder="Fase" autocomplete="off" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="inputTipo">Tipo</label>
                            <select class="form-control" id="inputTipo" name="tipo">
                                @switch(old('tipo'))
                                    @case('Secuencial')
                                        <option>Independiente</option>
                                        <option selected>Secuencial</option>
                                        @break

                                    @default
                                        <option selected>Independiente</option>
                                        <option>Secuencial</option>
                                @endswitch
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="inputFechaInicio">Fecha de Inicio</label>
                            <div class="input-group date" id="fechaInicio" data-target-input="nearest">
                                <input id="inputFechaInicio" type="text" name="fecha_inicio" value="{{ old('fecha_inicio') }}"  class="form-control datetimepicker-input" data-target="#fechaInicio">
                                <div class="input-group-append" data-target="#fechaInicio" data-toggle="datetimepicker">
                                    <div class="input-group-text"><i class="far fa-calendar-alt"></i></div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputFechaFinal">Fecha de Final</label>
                            <div class="input-group date" id="fechaFinal" data-target-input="nearest">
                                <input id="inputFechaFinal" type="text" name="fecha_final" value="{{ old('fecha_final') }}"  class="form-control datetimepicker-input" data-target="#fechaFinal">
                                <div class="input-group-append" data-target="#fechaFinal" data-toggle="datetimepicker">
                                    <div class="input-group-text"><i class="far fa-calendar-alt"></i></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 pl-0">
                                <p class="text-right">
                                    <button type="submit" class="btn btn-space btn-primary">Submit</button>
                                    <button class="btn btn-space btn-secondary"><a href="{{ route('usuarios.index') }}">Cancel</a></button>
                                </p>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end basic form -->
        <!-- ============================================================== -->
    </div>
@endsection