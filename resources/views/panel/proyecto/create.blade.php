@extends('panel.layout')

@section('contenido')
    <div class="row">
        <!-- ============================================================== -->
        <!-- basic form -->
        <!-- ============================================================== -->
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="card">
                <h5 class="card-header">Proyecto</h5>
                <div class="card-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <h6>Por favor corrige los errores debajo:</h6>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form method='POST' action="{{ url('proyectos') }}" id="basicform" data-parsley-validate="">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="inputTitulo">Titulo</label>
                            <input id="inputTitulo" type="text" name="titulo" value="{{ old('titulo') }}" data-parsley-trigger="change" placeholder="Titulo" autocomplete="off" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="inputDescripcion">Descripción</label>
                            <textarea class="form-control" name="descripcion" id="inputDescripcion" rows="3">
                                {{ old('descripcion') }}
                            </textarea>
                        </div>
                        <div class="form-group">
                            <label for="inputFechaEntrega">Fecha de Entrega</label>
                            <div class="input-group date" id="datetimepicker4" data-target-input="nearest">
                                <input id="inputFechaEntrega" type="text" name="fecha_entrega" value="{{ old('fecha_entrega') }}" class="form-control datetimepicker-input" data-target="#datetimepicker4">
                                <div class="input-group-append" data-target="#datetimepicker4" data-toggle="datetimepicker">
                                    <div class="input-group-text"><i class="far fa-calendar-alt"></i></div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputStack">Stack</label>
                            <input id="inputStack" type="text" name="stack" value="{{ old('stack') }}" data-parsley-trigger="change" placeholder="Stack" autocomplete="off" class="form-control">
                        </div>
                        <div class="row">
                            <div class="col-sm-12 pl-0">
                                <p class="text-right">
                                    <button type="submit" class="btn btn-space btn-primary">Submit</button>
                                    <button class="btn btn-space btn-secondary"><a href="{{ route('usuarios.index') }}">Cancel</a></button>
                                </p>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end basic form -->
        <!-- ============================================================== -->
    </div>
@endsection