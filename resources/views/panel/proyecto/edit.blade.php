@extends('panel.layout')

@section('contenido')
    <div class="row">
        <!-- ============================================================== -->
        <!-- basic form -->
        <!-- ============================================================== -->
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="card">
                <h5 class="card-header">Editar Tarea</h5>
                <div class="card-body">
                    <form method='POST' action="{{ url('proyecto/'.$proyecto->id) }}" id="basicform" data-parsley-validate="">
                        {{ method_field('PUT') }}
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="inputTitulo">Titulo</label>
                            <input value="{{ old('titulo', $proyecto->titulo) }}" id="inputTitulo" type="text" name="titulo" data-parsley-trigger="change" required="" placeholder="Titulo" autocomplete="off" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="inputDescripcion">Descripción</label>
                            <textarea class="form-control" name="descripcion" id="inputDescripcion" required="" rows="3">{{ old('descripcion', $proyecto->descripcion) }}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="inputFechaEntrega">Fecha de Entrega</label>
                            <div class="input-group date" id="fechaEntrega" data-target-input="nearest">
                                <input value="{{ old('fecha_entrega', $proyecto->fecha_entrega) }}" id="inputFechaEntrega" type="text" name="fecha_entrega"  class="form-control datetimepicker-input" required="" data-target="#fechaEntrega">
                                <div class="input-group-append" data-target="#fechaEntrega" data-toggle="datetimepicker">
                                    <div class="input-group-text"><i class="far fa-calendar-alt"></i></div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputStack">Stack</label>
                            <input value="{{ old('stack', $proyecto->stack) }}"t id="inputStack" type="text" name="stack" data-parsley-trigger="change" placeholder="Stack" required="" autocomplete="off" class="form-control">
                        </div>
                        <div class="row">
                            <div class="col-sm-12 pl-0">
                                <p class="text-right">
                                    <button type="submit" class="btn btn-space btn-primary">Submit</button>
                                    <button class="btn btn-space btn-secondary"><a href="{{ route('usuarios.index') }}">Cancel</a></button>
                                </p>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end basic form -->
        <!-- ============================================================== -->
    </div>
@endsection