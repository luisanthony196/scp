@extends('panel.layout')

@section('contenido')
    <div class="row">
        <!-- ============================================================== -->
        <!-- basic table -->
        <!-- ============================================================== -->
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="float-left">Proyectos</h5> 
                    <a href="{{ route('proyectos.create') }}" class="btn btn-success btn-xs float-right"><i class="fa fa-plus-circle"></i> Crear</a>
                </div>
                <div class="card-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Titulo</th>
                                <th scope="col">Descripcion</th>
                                <th scope="col">Fecha de Entrega</th>
                                <th scope="col">stack</th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($proyectos as $proyecto)
                                <tr>
                                    <th scope="row">{{ $proyecto->id }} </th>
                                    <td>{{ $proyecto->titulo }}</td>
                                    <td>
                                        <form action="">
                                            <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" readonly>{{ $proyecto->descripcion }}</textarea>
                                        </form>
                                    </td>
                                    <td> {{ date('d-m-Y', strtotime($proyecto->fecha_entrega)) }} </td>
                                    <td>{{ $proyecto->stack }}</td>
                                    <td>
                                        <a href="{{ route('proyectos.edit', ['id' => $proyecto->id]) }}" class="btn btn-primary btn-xs"># Editar</a>
                                        <form class="d-inline" action="{{ route('proyectos.destroy', ['id' => $proyecto->id]) }}" method="POST">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}
                                            <button type="submit" class="btn btn-secondary btn-xs">Eliminar</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end basic table -->
        <!-- ============================================================== -->
    </div>
@endsection