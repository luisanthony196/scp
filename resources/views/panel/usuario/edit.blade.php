@extends('panel.layout')

@section('contenido')
    <div class="row">
        <!-- ============================================================== -->
        <!-- basic form -->
        <!-- ============================================================== -->
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="card">
                <h5 class="card-header">Editar Usuario</h5>
                <div class="card-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <h6>Por favor corrige los errores debajo:</h6>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form method='POST' action="{{ url('usuario/'.$usuario->id) }}" id="basicform" data-parsley-validate="">
                        {{ method_field('PUT') }}
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="inputNombres">Nombres</label>
                            <input value="{{ old('nombres', $usuario->nombres) }}" id="inputNombres" type="text" name="nombres" data-parsley-trigger="change" placeholder="Nombres" autocomplete="off" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="inputApellidos">Apellidos</label>
                            <input value="{{ old('apellidos', $usuario->apellidos) }}" id="inputApellidos" type="text" name="apellidos" data-parsley-trigger="change" placeholder="Apellidos" autocomplete="off" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="inputPerfil">Perfil</label>
                            <select class="form-control" id="inputPerfil" name="perfil">
                                @switch($usuario->perfil)
                                    @case('Gestor')
                                        <option selected="selected">Gestor</option>
                                        <option>Colaborador</option>
                                        <option>Cliente</option>
                                        @break

                                    @case('Colaborador')
                                        <option>Gestor</option>
                                        <option selected="selected">Colaborador</option>
                                        <option>Cliente</option>
                                        @break

                                    @default
                                        <option>Gestor</option>
                                        <option>Colaborador</option>
                                        <option selected="selected">Cliente</option>
                                @endswitch
                            </select>
                        </div>
                        <div class="form-group d-none" id="empresa">
                            <label for="inputEmpresa">Empresa</label>
                            <input id="inputEmpresa" type="text" name="empresa" value="{{ old('empresa', $usuario->empresa) }}" data-parsley-trigger="change" placeholder="Nombre de la Empresa" autocomplete="off" class="form-control">
                        </div>
                        <script>
                            document.getElementById("inputPerfil").addEventListener("change", myFunction);
                            
                            function myFunction() {
                                var x = document.getElementById("inputPerfil");
                                switch (x.value){
                                    case 'Gestor':
                                    document.getElementById("empresa").classList.add('d-none');
                                    break;
                                    case 'Colaborador':
                                    document.getElementById("empresa").classList.add('d-none');
                                    break;
                                    case 'Cliente':
                                    document.getElementById("empresa").classList.remove('d-none');
                                    break;
                                }
                            }
                        </script>
                        <div class="form-group">
                            <label for="inputEmail">Email</label>
                            <input value="{{ old('email', $usuario->email) }}" id="inputEmail" type="text" name="email" data-parsley-trigger="change" placeholder="Ingresa tu Email" autocomplete="off" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="inputPassword">Contraseña</label>
                            <input id="inputPassword" type="password" name="contrasena" placeholder="Password" class="form-control">
                        </div>
                        <div class="row">
                            <div class="col-sm-12 pl-0">
                                <p class="text-right">
                                    <button type="submit" class="btn btn-space btn-primary">Submit</button>
                                    <button class="btn btn-space btn-secondary"><a href="{{ route('usuarios.index') }}">Cancel</a></button>
                                </p>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end basic form -->
        <!-- ============================================================== -->
    </div>
@endsection