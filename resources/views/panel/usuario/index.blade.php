@extends('panel.layout')

@section('contenido')
    <div class="row">
        <!-- ============================================================== -->
        <!-- basic table -->
        <!-- ============================================================== -->
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="float-left">Usuarios</h5> 
                    <a href="{{ route('usuarios.create') }}" class="btn btn-success btn-xs float-right"><i class="fa fa-plus-circle"></i> Crear</a>
                </div>
                <div class="card-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Nombres</th>
                                <th scope="col">Apellidos</th>
                                <th scope="col">Perfil</th>
                                <th scope="col">Email</th>
                                <th scope="col">Empresa</th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($usuarios as $usuario)
                                <tr>
                                    <th scope="row">{{ $usuario->id }} </th>
                                    <td>{{ $usuario->nombres }}</td>
                                    <td>{{ $usuario->apellidos }}</td>
                                    <td>{{ $usuario->perfil }}</td>
                                    <td>{{ $usuario->email }}</td>
                                    <td>{{ $usuario->empresa }}</td>
                                    <td>
                                        <a href="{{ route('usuarios.edit', ['id' => $usuario->id]) }}" class="btn btn-primary btn-xs"># Editar</a>
                                        <form class="d-inline" action="{{ route('usuarios.destroy', ['id' => $usuario->id]) }}" method="POST">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}
                                            <button type="submit" class="btn btn-secondary btn-xs">Eliminar</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end basic table -->
        <!-- ============================================================== -->
    </div>
@endsection