@extends('panel.layout')

@section('contenido')
    <div class="row">
        <!-- ============================================================== -->
        <!-- basic form -->
        <!-- ============================================================== -->
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="card">
                <h5 class="card-header">Usuario</h5>
                <div class="card-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <h6>Por favor corrige los errores debajo:</h6>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form method='POST' action="{{ url('usuarios') }}" id="basicform" data-parsley-validate="">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="inputNombres">Nombres</label>
                            <input id="inputNombres" type="text" name="nombres" value="{{ old('nombres') }}" data-parsley-trigger="change" placeholder="Nombres" autocomplete="off" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="inputApellidos">Apellidos</label>
                            <input id="inputApellidos" type="text" name="apellidos" value="{{ old('apellidos') }}" data-parsley-trigger="change" placeholder="Apellidos" autocomplete="off" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="input-select">Perfil</label>
                            <select class="form-control" id="input-select" name="perfil">
                                @switch(old('perfil'))
                                    @case('Colaborador')
                                        <option>Gestor</option>
                                        <option selected>Colaborador</option>
                                        <option>Cliente</option>
                                        @break

                                    @case('Cliente')
                                        <option>Gestor</option>
                                        <option>Colaborador</option>
                                        <option selected>Cliente</option>
                                        @break

                                    @default
                                        <option selected>Gestor</option>
                                        <option>Colaborador</option>
                                        <option>Cliente</option>
                                @endswitch
                            </select>
                        </div>
                        @if(old('perfil')=='Cliente')
                        <div class="form-group" id="empresa">
                        @else
                        <div class="form-group d-none" id="empresa">
                        @endif
                            <label for="inputEmpresa">Empresa</label>
                            <input id="inputEmpresa" type="text" name="empresa" value="{{ old('empresa') }}" data-parsley-trigger="change" placeholder="Empresa" autocomplete="off" class="form-control">
                        </div>
                        <script>
                            document.getElementById("input-select").addEventListener("change", myFunction);
                            
                            function myFunction() {
                                var x = document.getElementById("input-select");
                                switch (x.value){
                                    case 'Gestor':
                                    document.getElementById("inputEmpresa").value="";
                                    document.getElementById("empresa").classList.add('d-none');
                                    break;
                                    case 'Colaborador':
                                    document.getElementById("inputEmpresa").value="";
                                    document.getElementById("empresa").classList.add('d-none');
                                    break;
                                    case 'Cliente':
                                    document.getElementById("empresa").classList.remove('d-none');
                                    break;
                                }
                            }
                        </script>
                        <div class="form-group">
                            <label for="inputEmail">Email</label>
                            <input id="inputEmail" type="text" name="email" value="{{ old('email') }}" data-parsley-trigger="change" placeholder="Email" autocomplete="off" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="inputContrasena">Contraseña</label>
                            <input id="inputContrasena" type="password" name="contrasena" placeholder="Contraseña" class="form-control">
                        </div>
                        <div class="row">
                            <div class="col-sm-12 pl-0">
                                <p class="text-right">
                                    <button type="submit" class="btn btn-space btn-primary">Submit</button>
                                    <button class="btn btn-space btn-secondary"><a href="{{ route('usuarios.index') }}">Cancel</a></button>
                                </p>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end basic form -->
        <!-- ============================================================== -->
    </div>
@endsection