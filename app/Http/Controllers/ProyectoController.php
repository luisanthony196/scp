<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Proyecto;

class ProyectoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // dd(Proyecto::find(1)->tareas);
        $proyectos = Proyecto::get();
        return view('panel.proyecto.index', compact('proyectos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('panel.proyecto.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = request()->all();

        $data = request()->validate([
            'titulo' => 'required',
            'descripcion' => 'required',
            'fecha_entrega' => 'required',
            'stack' => 'required'
        ], [
            'titulo.required' => 'El campo titulo es obligatorio',
            'descripcion.required' => 'El campo descripcion es obligatorio',
            'fecha_entrega.required' => 'El campo fecha de entrega es obligatorio',
            'stack.required' => 'El campo stack es obligatorio'
        ]);

        Proyecto::create([
            'titulo' => $data['titulo'],
            'descripcion' => $data['descripcion'],
            'fecha_entrega' => $data['fecha_entrega'],
            'stack' => $data['stack'],
            'estado' => 1
        ]);
        return redirect()->route('proyectos.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $proyecto = Proyecto::find($id);
        return view('panel.proyecto.edit', ['proyecto' => $proyecto]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = request()->all();

        $data = request()->validate([
            'titulo' => 'required',
            'descripcion' => 'required',
            'fecha_entrega' => 'required',
            'stack' => 'required'
        ], [
            'titulo.required' => 'El campo titulo es obligatorio',
            'descripcion.required' => 'El campo descripcion es obligatorio',
            'fecha_entrega.required' => 'El campo fecha de entrega es obligatorio',
            'stack.required' => 'El campo stack es obligatorio'
        ]);
        $proyecto = Proyecto::findOrFail($id);
        $proyecto -> update($data);
        return redirect()->route('proyectos.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $proyecto = Proyecto::findOrFail($id);
        $proyecto->delete();
        return redirect()->route('proyectos.index');
    }
}
