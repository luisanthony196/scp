<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Usuario;

class UsuarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $usuarios = Usuario::get();
        return view('panel.usuario.index', compact('usuarios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('panel.usuario.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(request('perfil')=='Cliente'){
            $data = request()->validate([
                'nombres' => 'required',
                'apellidos' => 'required',
                'perfil' => '',
                'empresa' => 'required',
                'email' => ['required', 'email', 'unique:usuario,email'],
                'contrasena' => 'required'
            ], [
                'nombres.required' => 'El campo nombre es obligatorio',
                'apellidos.required' => 'El campo apellido es obligatorio',
                'empresa.required' => 'El campo empresa es obligatorio',
                'email.required' => 'El campo email es obligatorio',
                'contrasena.required' => 'El campo contraseña es obligatorio'
            ]);

            Usuario::create([
                'nombres' => $data['nombres'],
                'apellidos' => $data['apellidos'],
                'perfil' => $data['perfil'],
                'empresa' => $data['empresa'],
                'email' => $data['email'],
                'contrasena' => $data['contrasena'],
                'estado' => 1
            ]);
        }else{
            $data = request()->validate([
                'nombres' => 'required',
                'apellidos' => 'required',
                'perfil' => '',
                'email' => ['required', 'email', 'unique:usuario,email'],
                'contrasena' => 'required'
            ], [
                'nombres.required' => 'El campo nombre es obligatorio',
                'apellidos.required' => 'El campo apellido es obligatorio',
                'email.required' => 'El campo email es obligatorio',
                'contrasena.required' => 'El campo contraseña es obligatorio'
            ]);

            Usuario::create([
                'nombres' => $data['nombres'],
                'apellidos' => $data['apellidos'],
                'perfil' => $data['perfil'],
                'email' => $data['email'],
                'contrasena' => $data['contrasena'],
                'estado' => 1
            ]);
        }
        return redirect()->route('usuarios.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $usuario = Usuario::findOrFail($id);
        return view('panel.usuario.edit', ['usuario' => $usuario]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = request()->all();
        $usuario = Usuario::findOrFail($id);
        if($data['perfil']=='Cliente'){
            $data = request()->validate([
                'nombres' => 'required',
                'apellidos' => 'required',
                'perfil' => '',
                'empresa' => 'required',
                'email' => 'required|email',
                'contrasena' => 'required'
            ], [
                'nombres.required' => 'El campo nombre es obligatorio',
                'apellidos.required' => 'El campo apellido es obligatorio',
                'empresa.required' => 'El campo empresa es obligatorio',
                'email.required' => 'El campo email es obligatorio',
                'contrasena.required' => 'El campo contraseña es obligatorio'
            ]);
            // $usuario->nombres = $data['nombres'];
            // $usuario->apellidos = $data['apellidos'];
            // $usuario->perfil = $data['perfil'];
            // $usuario->empresa = $data['empresa'];
            // $usuario->email = $data['email'];
            // $usuario->contrasena = $data['contrasena'];
            $usuario->update($data);
        }else{
            $data = request()->validate([
                'nombres' => 'required',
                'apellidos' => 'required',
                'perfil' => '',
                'email' => 'required|email',
                'contrasena' => 'required'
            ], [
                'nombres.required' => 'El campo nombre es obligatorio',
                'apellidos.required' => 'El campo apellido es obligatorio',
                'email.required' => 'El campo email es obligatorio',
                'contrasena.required' => 'El campo contraseña es obligatorio'
            ]);
            // $usuario->nombres = $data['nombres'];
            // $usuario->apellidos = $data['apellidos'];
            // $usuario->perfil = $data['perfil'];
            // $usuario->email = $data['email'];
            // $usuario->contrasena = $data['contrasena'];
            $usuario->update($data);
        }
        return redirect()->route('usuarios.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $usuario = Usuario::findOrFail($id);
        $usuario->delete();
        return redirect()->route('usuarios.index');
    }
}
