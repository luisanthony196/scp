<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Tarea;

class TareaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // dd(Tarea::find(1)->proyecto);
        $tareas = Tarea::get();
        return view('panel.tarea.index', compact('tareas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('panel.tarea.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = request()->all();

        $data = request()->validate([
            'titulo' => 'required',
            'descripcion' => 'required',
            'prioridad' => 'required',
            'fase' => 'required',
            'tipo' => 'required',
            'fecha_inicio' => 'required',
            'fecha_final' => 'required'
        ], [
            'titulo.required' => 'El campo titulo es obligatorio',
            'descripcion.required' => 'El campo descripcion es obligatorio',
            'fase.required' => 'El campo fase es obligatorio',
            'fecha_inicio.required' => 'El campo fecha de inicio es obligatorio',
            'fecha_final.required' => 'El campo fecha final es obligatorio'
        ]);

        Tarea::create([
            'titulo' => $data['titulo'],
            'descripcion' => $data['descripcion'],
            'prioridad' => $data['prioridad'],
            'fase' => $data['fase'],
            'tipo' => $data['tipo'],
            'fecha_inicio' => $data['fecha_inicio'],
            'fecha_final' => $data['fecha_final'],
            'estado' => 1
        ]);
        return redirect()->route('proyectos.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tarea = Tarea::find($id);
        return view('panel.tarea.edit', ['tarea' => $tarea]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = request()->all();

        $data = request()->validate([
            'titulo' => 'required',
            'descripcion' => 'required',
            'prioridad' => 'required',
            'fase' => 'required',
            'tipo' => 'required',
            'fecha_inicio' => 'required',
            'fecha_final' => 'required'
        ], [
            'titulo.required' => 'El campo titulo es obligatorio',
            'descripcion.required' => 'El campo descripcion es obligatorio',
            'fase.required' => 'El campo fase es obligatorio',
            'fecha_inicio.required' => 'El campo fecha de inicio es obligatorio',
            'fecha_final.required' => 'El campo fecha final es obligatorio'
        ]);
        $tarea = Tarea::findOrFail($id);
        $tarea -> update($data);
        return redirect()->route('tareas.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tarea = Tarea::findOrFail($id);
        $tarea->delete();
        return redirect()->route('tareas.index');
    }
}
