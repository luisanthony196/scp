<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Usuario;
use App\Models\Proyecto;
use App\Models\Tarea;

class ResumenController extends Controller
{
    public function index(){

        $cant_usu = Usuario::get()->count();
        $cant_pro = Proyecto::get()->count();
        $cant_tar = Tarea::get()->count();

        return view('panel.index', compact('cant_usu','cant_pro','cant_tar'));
    }
}
