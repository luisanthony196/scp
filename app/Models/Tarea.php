<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tarea extends Model
{
    protected $table = 'tarea';
    protected $fillable = ['titulo', 'descripcion', 'prioridad', 'fase', 'tipo', 'fecha_inicio', 'fecha_final', 'estado'];
    protected $dates = ['fecha_inicio', 'fecha_final'];

    public function proyecto()
    {
        return $this->belongsTo('App\Models\Proyecto');
    }

    public function usuarios()
    {
        return $this->belongsToMany('App\Models\Usuario');
    }
}
