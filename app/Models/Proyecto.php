<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Proyecto extends Model
{
    protected $table = 'proyecto';
    protected $fillable = ['titulo', 'descripcion', 'fecha_entrega', 'stack', 'estado'];
    protected $dates = ['fecha_entrega'];

    public function tareas()
    {
        return $this->hasMany('App\Models\Tarea');
    }
}
