<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
    protected $table = 'usuario';
    protected $fillable = ['nombres', 'apellidos', 'perfil', 'email', 'contrasena', 'estado'];

    public function tareas()
    {
        return $this->belongsToMany('App\Models\Tarea');
    }
}
