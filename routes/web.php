<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Home
Route::get('/', 'ResumenController@index')->middleware('auth');;

// Proyectos
Route::get('proyectos', 'ProyectoController@index')->name('proyectos.index')->middleware('auth');;
Route::get('proyectos/nuevo', 'ProyectoController@create')->name('proyectos.create')->middleware('auth');;
Route::post('proyectos', 'ProyectoController@store');
Route::get('proyecto/{id}', 'ProyectoController@edit')->name('proyectos.edit')->middleware('auth');;
Route::put('proyecto/{id}', 'ProyectoController@update');
Route::delete('proyecto/{id}', 'ProyectoController@destroy')->name('proyectos.destroy');

// Usuarios
Route::get('usuarios', 'UsuarioController@index')->name('usuarios.index')->middleware('auth');;
Route::get('usuarios/nuevo', 'UsuarioController@create')->name('usuarios.create')->middleware('auth');;
Route::post('usuarios', 'UsuarioController@store');
Route::get('usuario/{id}', 'UsuarioController@edit')->name('usuarios.edit')->middleware('auth');;
Route::put('usuario/{id}', 'UsuarioController@update');
Route::delete('usuario/{id}', 'UsuarioController@destroy')->name('usuarios.destroy');

// Tareas
Route::get('tareas', 'TareaController@index')->name('tareas.index')->middleware('auth');;
Route::get('nuevotar', 'TareaController@create')->name('tareas.create')->middleware('auth');;
Route::post('tareas', 'TareaController@store');
Route::get('tarea/{id}', 'TareaController@edit')->name('tareas.edit')->middleware('auth');;
Route::put('tarea/{id}', 'TareaController@update');
Route::delete('tarea/{id}', 'TareaController@destroy')->name('tareas.destroy');

//Auth
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
